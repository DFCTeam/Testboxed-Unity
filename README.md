# Testboxed Unity
Official Testboxed source code and assets

# System requirements (Editor)
Any OS with Unity Editor 2023.XX

TextMeshPro in project

Mirror Networking lib in project (86.13.4 or higher)

# Special thanks to...
[Unity](https://unity.com/) - Game Engine, Testboxed is based on this

[Apple](https://apple.com/) - macOS, iOS, and other platforms. Thanks for iPhone 12 mini

[Adobe](https://www.adobe.com/) - Adobe Illustrator is great

[Kenney](https://www.kenney.nl/) - Some textures

[Kawase Blur](https://github.com/tomc128/urp-kawase-blur) - Blur for UI (I didn't use it)

[Changed](https://store.steampowered.com/app/814540/Changed/) - I borrowed a lot of the textures from there. And I love furry

[s&box](https://asset.party/) - The idea to create Testboxed, I copied the main menu from there