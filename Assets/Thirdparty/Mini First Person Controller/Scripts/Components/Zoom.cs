﻿using UnityEngine;

[ExecuteInEditMode]
public class Zoom : MonoBehaviour
{
    Camera camera;
    public float defaultFOV = 60;
    public float maxZoomFOV = 15;
    [Range(0, 1)]
    public float currentZoom;
    public float sensitivity = 1;
    public float zoomSmoothTime = 0.2f; // Время для плавного изменения зума
    private float zoomVelocity; // Скорость изменения зума

    void Awake()
    {
        // Получаем камеру на этом объекте и устанавливаем defaultFOV.
        camera = GetComponent<Camera>();
        if (camera)
        {
            defaultFOV = camera.fieldOfView;
        }
    }

    void Update()
    {
        // Обновляем текущий зум и поле зрения камеры.
        currentZoom += Input.mouseScrollDelta.y * sensitivity * .05f;
        currentZoom = Mathf.Clamp01(currentZoom);

        // Используем SmoothDamp для плавного изменения зума.
        float targetFOV = Mathf.Lerp(defaultFOV, maxZoomFOV, currentZoom);
        camera.fieldOfView = Mathf.SmoothDamp(camera.fieldOfView, targetFOV, ref zoomVelocity, zoomSmoothTime);
    }
}
