using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromoUIShow : MonoBehaviour
{
    [SerializeField] private GameObject gj;
    [SerializeField] private string varName;

    private void Start()
    {
        int state = PlayerPrefs.GetInt(varName, 0);
        if (state == 0) {
            gj.SetActive(true);
        }
    }
}
