using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToogleOnKeyClick : MonoBehaviour
{
    [SerializeField] private GameObject objToToogle;
    [SerializeField] private KeyCode key;

    private void Update()
    {
        if (Input.GetKeyDown(key)) {
            objToToogle.SetActive(!objToToogle.activeSelf);
        }
    }
}
