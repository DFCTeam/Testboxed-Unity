using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class EvalLua : MonoBehaviour
{
    // private void Start()
    // {
    //     // Logging
    //     Script.DefaultOptions.DebugPrint = s => Debug.Log(s);

    //     // Bindings
    //     // TODO: Implement it
    // }

    public static DynValue EvalString(string luaScript)
    {
        Script.DefaultOptions.DebugPrint = s => Debug.Log(s);
        DynValue res = Script.RunString(luaScript);
        return res;
    }
}
