using UnityEngine;

public class DontDestroyOnLoadManager : MonoBehaviour
{
    // Список объектов, которые мы хотим сохранить при переходе между сценами
    [SerializeField] private GameObject[] objectsToKeep;

    // Вызывается при загрузке уровня
    private void Start()
    {
        // Проходим по всем объектам в списке
        foreach (GameObject obj in objectsToKeep)
        {
            // Помечаем их как "dont destroy on load"
            DontDestroyOnLoad(obj);
        }
    }
}
