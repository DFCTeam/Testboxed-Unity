using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class Chat : NetworkBehaviour
{
    public TMP_InputField inputField;
    public TMP_Text chatText;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SendChatMessage();
        }
    }

    [Command(requiresAuthority = false)]
    private void CmdSendChatMessage(string message)
    {
        RpcReceiveChatMessage(message);
    }

    [ClientRpc]
    private void RpcReceiveChatMessage(string message)
    {
        StartCoroutine(ShowMessage(message));
    }

    private void SendChatMessage()
    {
        string message = inputField.text;
        if (!string.IsNullOrWhiteSpace(message))
        {
            string username = PlayerPrefs.GetString("PlayerUsername", "");
            int color = PlayerPrefs.GetInt("PlayerColor", 0);
            string color2 = "";

            if (color == 1) { color2 = "#EB4034"; }
            if (color == 2) { color2 = "#EBD634"; }
            if (color == 3) { color2 = "#EB8334"; }
            if (color == 4) { color2 = "#89EB34"; }
            if (color == 5) { color2 = "#348FEB"; }

            CmdSendChatMessage($"<color={color2}>{username}</color>: {message}");
            inputField.text = string.Empty;
        }
    }

    private IEnumerator ShowMessage(string message)
    {
        chatText.text += $"\n{message}";
        yield return null;
    }
}
