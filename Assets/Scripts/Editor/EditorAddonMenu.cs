using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class AddonMenu : EditorWindow
{
    [MenuItem("Testboxed Utilities/Open Addons Folder")]
    private static void OpenAddonsFolder()
    {
        string addonsFolderPath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, "Addons"));
        Debug.Log(addonsFolderPath);
        EditorUtility.RevealInFinder(addonsFolderPath);
    }

    [MenuItem("Testboxed Utilities/Delete PlayerPrefs")]
    private static void DeletePlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}