using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableGameObjectsOnStartAndAfter : MonoBehaviour
{
    public GameObject[] gameObjects;

    private void Start()
    {
        Invoke("EnableObjects", 2f);
    }

    private void EnableObjects()
    {
        foreach (GameObject obj in gameObjects)
        {
            obj.SetActive(true);
        }
    }
}
