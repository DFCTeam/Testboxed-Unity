using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoonSharp.Interpreter;

public class UIEvalLua : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_InputField textInput;
    [SerializeField] private Button evalButton;

    private void Start()
    {
        Script.DefaultOptions.DebugPrint = s => Debug.Log(s);
    	evalButton.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        string luaScript = textInput.text;

        EvalLua.EvalString(luaScript);

        Debug.Log("Lua code runned");
    }
}