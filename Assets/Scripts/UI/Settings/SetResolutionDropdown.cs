using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetResolutionDropdown : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown resolutionDropdown; // перетащи Dropdown из твоего UI сюда

    private void Start()
    {
        // Получаем доступ к доступным разрешениям экрана
        Resolution[] resolutions = Screen.resolutions;

        // Очищаем Dropdown перед добавлением новых опций
        resolutionDropdown.ClearOptions();

        // Создаем список строк для опций Dropdown
        var resolutionOptions = new List<TMP_Dropdown.OptionData>();

        // Добавляем разрешения в список
        foreach (Resolution resolution in resolutions)
        {
            string optionText = resolution.width + " x " + resolution.height + " x " + resolution.refreshRateRatio;
            TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData(optionText);
            resolutionOptions.Add(optionData);
        }

        // Добавляем опции в Dropdown
        resolutionDropdown.AddOptions(resolutionOptions);

        // Добавляем слушателя на событие изменения Dropdown
        resolutionDropdown.onValueChanged.AddListener(OnResolutionDropdownValueChanged);
    }

    // Обработчик изменения разрешения в Dropdown
    private void OnResolutionDropdownValueChanged(int resolutionIndex)
    {
        // Получаем доступ к доступным разрешениям экрана
        Resolution[] resolutions = Screen.resolutions;

        // Проверяем, чтобы индекс не выходил за пределы массива
        if (resolutionIndex >= 0 && resolutionIndex < resolutions.Length)
        {
            // Устанавливаем выбранное разрешение
            Screen.SetResolution(
            	resolutions[resolutionIndex].width,
            	resolutions[resolutionIndex].height,
            	FullScreenMode.FullScreenWindow,
            	new RefreshRate() {
            		numerator = resolutions[resolutionIndex].refreshRateRatio.numerator,
            		denominator = 1
            	}
            );
        }
    }
}
