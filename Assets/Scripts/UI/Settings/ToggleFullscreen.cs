using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleFullscreen : MonoBehaviour
{
    [SerializeField] private Toggle fullscreenToggle; // перетащи Toggle из твоего UI сюда

    private void Start()
    {
        // Проверим текущий режим fullscreen и установим Toggle в нужное положение
        fullscreenToggle.isOn = Screen.fullScreen;

        // Добавим слушателя на событие изменения Toggle
        fullscreenToggle.onValueChanged.AddListener(OnFullscreenToggleValueChanged);
    }

    // Обработчик изменения Toggle
    private void OnFullscreenToggleValueChanged(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen; // Изменяем режим fullscreen при изменении Toggle
    }
}
