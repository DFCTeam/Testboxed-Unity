using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTimescaleOnSlider : MonoBehaviour
{
    [SerializeField] private Slider slider;

    private void Update()
    {
        Time.timeScale = slider.value;
    }
}
