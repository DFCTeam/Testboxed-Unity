using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Management;

public class EnableXR : MonoBehaviour
{
    private Toggle m_Toggle;

    private IEnumerator LoadDevice(string newDevice, bool enable)
	{
	    XRSettings.LoadDeviceByName(newDevice);
	    yield return null;
	    XRSettings.enabled = enable;
	}

    private void Enable()
    {
    	XRGeneralSettings.Instance.Manager.InitializeLoader();
    	if (XRGeneralSettings.Instance.Manager.activeLoader != null) {
    		StartCoroutine(LoadDevice(XRSettings.supportedDevices[0], true));
    		XRGeneralSettings.Instance.Manager.StartSubsystems();
    	}
    	else {
    		Debug.LogWarning("activeLoader is null, XR not loaded");
    	}
    }

    private void Disable()
    {
    	StartCoroutine(LoadDevice("", false));
    	XRGeneralSettings.Instance.Manager.StopSubsystems();
        XRGeneralSettings.Instance.Manager.DeinitializeLoader();
    }

    private void Start()
    {
        m_Toggle = GetComponent<Toggle>();

        m_Toggle.onValueChanged.AddListener(delegate {
            ToggleValueChanged(m_Toggle);
        });
    }

    private void ToggleValueChanged(Toggle change)
    {
        // deprecated: XRSettings.enabled = m_Toggle.isOn;
        if (m_Toggle == true) {
        	Enable();
        }
        else {
        	Disable();
        }
    }
}
