using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class LanguageDropdown : MonoBehaviour
{
    public TMP_Dropdown languageDropdown;
    private List<LanguageData> languages = new List<LanguageData>();

    [System.Serializable]
    public class LanguageData
    {
        public string code;
        public string name;
        public string native;
    }

    private void Start()
    {
        LoadLanguages();
        PopulateDropdown();
    }

    private void LoadLanguages()
    {
        TextAsset jsonFile = Resources.Load<TextAsset>("languages");
        if (jsonFile != null)
        {
            LanguageList languageList = JsonUtility.FromJson<LanguageList>(jsonFile.text);
            languages = languageList.languages;
        }
        else
        {
            Debug.LogError("Unable to load languages.json. Make sure the file is in the Resources folder.");
        }
    }

    private void PopulateDropdown()
    {
        if (languageDropdown != null)
        {
            languageDropdown.ClearOptions();

            List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();

            foreach (LanguageData language in languages)
            {
                TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData();
                option.text = $"{language.name} ({language.native})";
                options.Add(option);
            }

            languageDropdown.AddOptions(options);
        }
        else
        {
            Debug.LogError("Dropdown component not assigned.");
        }
    }

    [System.Serializable]
    public class LanguageList
    {
        public List<LanguageData> languages;
    }
}
