using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadDebugReadOnly : MonoBehaviour
{
    [SerializeField] private Text propertiesText;

    private void Start()
    {
        string properties = "";

        properties += "Directory.GetCurrentDirectory(): " + Directory.GetCurrentDirectory() + "\n";
        properties += "\n";
        properties += "absoluteURL: " + Application.absoluteURL + "\n";
        properties += "backgroundLoadingPriority: " + Application.backgroundLoadingPriority + "\n";
        properties += "buildGUID: " + Application.buildGUID + "\n";
        properties += "cloudProjectId: " + Application.cloudProjectId + "\n";
        properties += "companyName: " + Application.companyName + "\n";
        properties += "consoleLogPath: " + Application.consoleLogPath + "\n";
        properties += "dataPath: " + Application.dataPath + "\n";
        properties += "exitCancellationToken: " + Application.exitCancellationToken + "\n";
        properties += "genuine: " + Application.genuine + "\n";
        properties += "genuineCheckAvailable: " + Application.genuineCheckAvailable + "\n";
        properties += "identifier: " + Application.identifier + "\n";
        properties += "installerName: " + Application.installerName + "\n";
        properties += "installMode: " + Application.installMode + "\n";
        properties += "internetReachability: " + Application.internetReachability + "\n";
        properties += "\n";
        properties += "isBatchMode: " + Application.isBatchMode + "\n";
        properties += "isConsolePlatform: " + Application.isConsolePlatform + "\n";
        properties += "isEditor: " + Application.isEditor + "\n";
        properties += "isFocused: " + Application.isFocused + "\n";
        properties += "isMobilePlatform: " + Application.isMobilePlatform + "\n";
        properties += "isPlaying: " + Application.isPlaying + "\n";
        properties += "\n";
        properties += "persistentDataPath: " + Application.persistentDataPath + "\n";
        properties += "platform: " + Application.platform + "\n";
        properties += "productName: " + Application.productName + "\n";
        properties += "runInBackground: " + Application.runInBackground + "\n";
        properties += "sandboxType: " + Application.sandboxType + "\n";
        properties += "streamingAssetsPath: " + Application.streamingAssetsPath + "\n";
        properties += "systemLanguage: " + Application.systemLanguage + "\n";
        properties += "targetFrameRate: " + Application.targetFrameRate + "\n";
        properties += "temporaryCachePath: " + Application.temporaryCachePath + "\n";
        properties += "unityVersion: " + Application.unityVersion + "\n";
        properties += "version: " + Application.version + "\n";
        properties += "\n";
        properties += "GetStackTraceLogType(): " + Application.GetStackTraceLogType(LogType.Log) + "\n";
        properties += "HasProLicense(): " + Application.HasProLicense() + "\n";

        propertiesText.text = properties;
    }
}
