using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ClickUIWithLockedCursor : MonoBehaviour
{
    public Canvas worldSpaceCanvas; // Reference to your World Space Canvas

    private void Update()
    {
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            if (Input.GetMouseButtonDown(0))
            {
                // Create a Ray from the mouse cursor position
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                // Check if the Ray hits an object
                if (Physics.Raycast(ray, out hit))
                {
                    // Check if the hit object is part of UI
                    var graphic = hit.collider.gameObject.GetComponent<Graphic>();
                    if (graphic != null)
                    {
                        // Execute a pointer click event on the UI element
                        ExecuteEvents.Execute(graphic.gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
                    }
                }
            }
        }
    }
}
