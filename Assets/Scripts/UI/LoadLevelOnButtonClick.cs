using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadLevelOnButtonClick : MonoBehaviour
{
    [SerializeField] private Button btn;
    [SerializeField] private string sceneName;

    private void Start()
    {
        btn.onClick.AddListener(onClick);
    }

    private void onClick()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadSceneAsync(sceneName);
    }
}
