using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class TextureViewerUI : MonoBehaviour
{
    public Image imageComponent; // Ссылка на компонент Image
    public TMPro.TMP_Dropdown dropdownComponent; // Ссылка на компонент TextMeshPro Dropdown

    private List<Sprite> sprites = new List<Sprite>(); // Список спрайтов для отображения
    private List<string> filenames = new List<string>();

    // Инициализация
    private void Start()
    {
        string[] folders = {
            "Assets/Textures/Colors",
            "Assets/Textures/Games/Changed/Characters",
        	"Assets/Textures/Games/Changed/Characters/10",
        	"Assets/Textures/Games/Changed/Characters/11",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Dark",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Red",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Green",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Light",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Orange",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Purple",
        	"Assets/Textures/Kenney/kenney_prototype-textures/PNG/Dark",
        	"Assets/Textures/Logos",
        	"Assets/Textures/Logos/TBLegacy",
        	"Assets/Textures/Logos/TBWordmarks",
        	"Assets/Textures/Logos/NonTBLogos",
        	"Assets/Textures/Screenshots",
        	"Assets/Textures/UI",
        };

        LoadSpritesFromFolders(folders);
        if (sprites.Count > 0)
        {
            InitializeDropdown();
        }
    }

    // Загрузка спрайтов из нескольких папок
    private void LoadSpritesFromFolders(string[] folderPaths)
    {
        foreach (string folderPath in folderPaths)
        {
            LoadSpritesFromFolder(folderPath);
        }
    }

    // Загрузка спрайтов из одной папки
    private void LoadSpritesFromFolder(string folderPath)
    {
        if (Directory.Exists(folderPath))
        {
            string[] files = Directory.GetFiles(folderPath, "*.png"); // Предполагаем, что в папке только PNG-файлы, можно изменить расширение

            foreach (string file in files)
            {
                byte[] fileData = File.ReadAllBytes(file);
                Texture2D texture = new Texture2D(2, 2);
                texture.LoadImage(fileData);

                filenames.Add(file);

                sprites.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f));
            }
        }
        else
        {
            Debug.LogError("Folder does not exist: " + folderPath);
        }
    }

    // Инициализация выпадающего списка со спрайтами
    private void InitializeDropdown()
    {
        if (dropdownComponent != null)
        {
            dropdownComponent.ClearOptions();

            // Создание списка опций для выпадающего списка
            List<TMPro.TMP_Dropdown.OptionData> options = new List<TMPro.TMP_Dropdown.OptionData>();
            for (int i = 0; i < sprites.Count; i++)
            {
                options.Add(new TMPro.TMP_Dropdown.OptionData(filenames[i]));
            }

            // Добавление списка опций к выпадающему списку
            dropdownComponent.ClearOptions();
            dropdownComponent.AddOptions(options);

            // Установка обработчика изменения значения в выпадающем списке
            dropdownComponent.onValueChanged.AddListener(OnDropdownValueChanged);
        }
    }

    // Обработчик изменения значения в выпадающем списке
    private void OnDropdownValueChanged(int index)
    {
        if (imageComponent != null)
        {
            if (index >= 0 && index < sprites.Count)
            {
                // Установка выбранного спрайта в компонент Image
                imageComponent.sprite = sprites[index];
            }
        }
    }
}