using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleOnButtonClickCursorToggle : MonoBehaviour
{
    [SerializeField] private KeyCode keyCode;
    [SerializeField] private GameObject objToToogle;

    private void Update()
    {
        if (!Input.GetKeyDown(keyCode)) return;

        objToToogle.SetActive(!objToToogle.activeSelf);
        if (objToToogle.activeSelf) {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
