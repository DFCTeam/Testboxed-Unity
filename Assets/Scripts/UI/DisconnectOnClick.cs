using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class DisconnectOnClick : MonoBehaviour
{
	[SerializeField] private Button disconnectButton;

    // Метод, вызываемый при старте
    private void Start()
    {
        // Назначаем метод Disconnect при нажатии на кнопку
        disconnectButton.onClick.AddListener(Disconnect);
    }

    // Метод для отключения от сервера
    private void Disconnect()
    {
        // Проверяем, подключены ли мы к серверу
        if (NetworkClient.isConnected)
        {
            // Отключаемся от сервера
            NetworkClient.Disconnect();
            Debug.Log("Отключились от сервера!");
        }
        else
        {
            Debug.Log("Не подключены к серверу!");
        }
    }
}
