using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;

public class StartServerOnButtonClick : MonoBehaviour
{
    [SerializeField] private Button startServerButton;
    [SerializeField] private TMP_InputField maxPlayers;
    [SerializeField] private TMP_InputField sendRateSrv;
    private int maxPlayersInternal;
    private int sendRateSrvInternal;

    private void Start()
    {
        startServerButton.onClick.AddListener(StartServer);
    }

    private void StartServer()
    {
        // Parsing values
        int.TryParse(maxPlayers.text, out maxPlayersInternal);
        int.TryParse(sendRateSrv.text, out sendRateSrvInternal);
        // Settings
        NetworkManager.singleton.maxConnections = maxPlayersInternal;
        NetworkManager.singleton.sendRate = sendRateSrvInternal;
        // Start host
        NetworkManager.singleton.StartHost();
    }
}
