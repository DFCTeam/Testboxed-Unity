using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class SpawnMenuItems : NetworkBehaviour
{
    [SerializeField] private Button[] buttons;
    [SerializeField] private GameObject[] prefabs;

    private void Start()
    {
        foreach (Button btn in buttons)
        {
            btn.onClick.AddListener(() =>
            {
                onClick(btn);
            });
        }
    }

    private void onClick(Button btn)
    {
        int index = System.Array.IndexOf(buttons, btn);

        if (index >= 0 && index < prefabs.Length)
        {
            // Получаем ссылку на игрока
            GameObject player = NetworkClient.localPlayer.gameObject;

            // Рассчитываем позицию для спауна, например, смещаем относительно позиции игрока
            Vector3 spawnPosition = player.transform.position + player.transform.up * 2f;

            // Спауним объект на сервере
            GameObject spawnedPrefab = Instantiate(prefabs[index], spawnPosition, Quaternion.identity);
            // Синхронизируем создание объекта с клиентами
            NetworkServer.Spawn(spawnedPrefab);
        }
        else
        {
            Debug.LogError("Invalid index or prefab not assigned.");
        }
    }
}