using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleOnButtonClick : MonoBehaviour
{
    [SerializeField] private Button btn;
    [SerializeField] private GameObject objToToogle;

    private void Start()
    {
        btn.onClick.AddListener(onClick);
    }

    private void onClick()
    {
        objToToogle.SetActive(!objToToogle.activeSelf);
    }
}
