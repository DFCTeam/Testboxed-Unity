using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using TMPro;

public class JoinByIP : MonoBehaviour
{
    [SerializeField] private TMP_InputField ipAddressInput;
    [SerializeField] private Button joinButton;
    [SerializeField] private GameObject joinUI;

    private void Start()
    {
        joinButton.onClick.AddListener(JoinServer);
    }

    private void JoinServer()
    {
        string ipAddress = ipAddressInput.text;
        
        if (!string.IsNullOrEmpty(ipAddress))
        {
            NetworkManager.singleton.networkAddress = ipAddress;
            NetworkManager.singleton.StartClient(); // Подключение к серверу по указанному IP
            Debug.Log("Trying to connect to: " + ipAddress);
            joinUI.SetActive(false);
        }
        else
        {
            Debug.Log("Please enter a valid IP address!");
            joinUI.SetActive(true);
        }
    }
}
