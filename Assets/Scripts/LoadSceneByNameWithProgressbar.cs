using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneByNameWithProgressbar : MonoBehaviour
{
    [SerializeField] private string sceneName;
    [SerializeField] private Slider slider;
    private AsyncOperation asyncLoad;

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(5);
        asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;
    }

    private void Start()
    {
        StartCoroutine(Delay());
    }

    private void Update()
    {
        if (asyncLoad != null)  // Добавили проверку на null
        {
            Debug.Log(asyncLoad.progress);
            slider.value = asyncLoad.progress;

            if (asyncLoad.progress >= 0.9f) {
            	asyncLoad.allowSceneActivation = true;
            }
        }
    }
}
