using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour
{
    [SerializeField] private GameObject prefabToSpawn;
    [SerializeField] private float spawnInterval = 0.02f;

    private void Start()
    {
        // Вызываем метод SpawnObject каждые spawnInterval секунд
        InvokeRepeating("SpawnObject", 0f, spawnInterval);
    }

    private void SpawnObject()
    {
        // Создаем экземпляр префаба в текущей позиции Spawner'а
        Instantiate(prefabToSpawn, transform.position, Quaternion.identity);
    }
}