using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class NetworkManagerHandler : NetworkManager
{
    public TMP_Text chatText;

    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        base.OnServerConnect(conn);

        Debug.Log("Player connected with connection id: " + conn.connectionId);
        //chatText.text += $"\n<color=#9234eb>NetworkManager</color>.<color=#9234eb>OnServerConnect</color>: Player connected with connection id: {conn.connectionId}";
    }

    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        base.OnServerDisconnect(conn);

        Debug.Log("Player connected with connection id: " + conn.connectionId);
        //chatText.text += $"\n<color=#9234eb>NetworkManager</color>.<color=#9234eb>OnServerDisconnect</color>: Player disconnected with connection id: {conn.connectionId}";
    }

    // public override void OnServerReady(NetworkConnectionToClient conn)
    // {
    //     base.OnServerReady(conn);

    //     //Debug.Log("Player connected with connection id: " + conn.connectionId);
    //     chatText.text += $"\n<color=#9234eb>NetworkManager</color>.<color=#9234eb>OnServerReady</color>: Player ready with connection id: {conn.connectionId}";
    // }


    // public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    // {
    //     base.OnServerAddPlayer(conn);

    //     //Debug.Log("Player connected with connection id: " + conn.connectionId);
    //     chatText.text += $"\n<color=#9234eb>NetworkManager</color>.<color=#9234eb>OnServerAddPlayer</color>: Player added with connection id: {conn.connectionId}";
    // }

    // public override void OnClientConnect()
    // {
    //     base.OnClientConnect();
    // }

    // public override void OnServerError(
    //     NetworkConnectionToClient   conn,
    //     TransportError      error,
    //     string      reason
    // )
    // {
    //     chatText.text += $"\n<color=#9234eb>NetworkManager</color>.<color=#9234eb>OnServerError</color>: <color=#EB4034>Error</color>: conn.connectionId = {conn.connectionId}; reason = {reason}";
    // }
}