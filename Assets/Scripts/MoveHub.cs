using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHub : MonoBehaviour
{
    private bool isDragging = false;
    private Vector3 offset;

    private void OnMouseDown()
    {
        isDragging = true;
        offset = transform.position - GetMouseWorldPosition();
    }

    private void OnMouseUp()
    {
        isDragging = false;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0)) // Проверяем, зажата ли левая кнопка мыши
        {
            if (isDragging)
            {
                Vector3 mousePos = GetMouseWorldPosition();
                transform.position = mousePos + offset;
            }
        }
        else
        {
            isDragging = false; // Если кнопка мыши не зажата, останавливаем перемещение
        }
    }

    private Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = Camera.main.nearClipPlane;
        return Camera.main.ScreenToWorldPoint(mousePosition);
    }
}