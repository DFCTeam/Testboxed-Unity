using UnityEngine;
using Mirror;

public class LookAtPlayer : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnRotationChanged))]
    private Quaternion playerRotation;

    [SerializeField] private Vector3 rotationOffset;

    private void Update()
    {
        // Получаем локального игрока через NetworkClient
        Transform player = NetworkClient.localPlayer?.transform;
        if (player != null)
        {
            // Находим вектор, указывающий на игрока
            Vector3 lookDirection = player.position - transform.position;
            // Используем Quaternion.LookRotation, чтобы повернуть объект к игроку
            playerRotation = Quaternion.LookRotation(lookDirection);
        }
    }

    private void OnRotationChanged(Quaternion oldRotation, Quaternion newRotation)
    {
        // Применяем поворот к объекту только по Y-оси
        transform.rotation = Quaternion.Euler(0f, newRotation.eulerAngles.y + rotationOffset.y, 0f);
    }
}