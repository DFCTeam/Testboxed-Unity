using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DetectDedicatedServer : MonoBehaviour
{
    private UnityEngine.RuntimePlatform[] arrServer = new UnityEngine.RuntimePlatform[3];

    private void Start()
    {
        arrServer[0] = UnityEngine.RuntimePlatform.WindowsServer;
        arrServer[1] = UnityEngine.RuntimePlatform.LinuxServer;
        arrServer[2] = UnityEngine.RuntimePlatform.OSXServer;

        if (Array.IndexOf(arrServer, Application.platform) != -1) {
            Debug.Log(@"
########################################
########################################
########################################
######          ####         ###########
######          ####   ######   ########
##########   #######   ######   ########
##########   #######         ###########
##########   #######         ###########
##########   #######   ######   ########
##########   #######   ######   ########
##########   #######         ###########
########################################
########################################
########################################
########################################
########################################
########################################");
            Debug.Log("\n\nWelcome to Testboxed Server!");
            Debug.Log($@"
[DEBUG START]
Unity version: {Application.unityVersion}
TB version: {Application.version}
Running in sandbox: {Application.sandboxType}
Platform: {Application.platform}
dataPath: {Application.dataPath}
[DEBUG END]
            ");
            Debug.Log("\n\nLoading Sandbox...");
            SceneManager.LoadSceneAsync("Sandbox");
        }
    }
}
