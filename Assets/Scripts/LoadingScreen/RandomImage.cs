using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomImage : MonoBehaviour
{
    // Список изображений
    [SerializeField] private Sprite[] imageList;

    // Список текста для каждого изображения
    [SerializeField] private string[] textList;

    // Ссылка на объект UI.Image
    [SerializeField] private Image imageComponent;

    // Ссылка на объект Text для отображения текста
    [SerializeField] private Text textComponent;

    private void Start()
    {
        // Проверка наличия ссылок на UI.Image и Text
        if (imageComponent == null || textComponent == null)
        {
            Debug.LogError("UI.Image или Text не присвоены!");
            return;
        }

        // Проверка наличия изображений и текста в списках
        if (imageList == null || imageList.Length == 0 || textList == null || textList.Length == 0)
        {
            Debug.LogError("Список изображений или текста пуст или не инициализирован!");
            return;
        }

        // Вызываем метод для изменения изображения и текста при запуске
        ChangeImageAndText();
    }

    // Метод для изменения изображения и текста
    private void ChangeImageAndText()
    {
        // Выбираем случайное изображение из списка
        int randomIndex = Random.Range(0, imageList.Length);
        Sprite randomImage = imageList[randomIndex];
        string randomText = textList[randomIndex];

        // Устанавливаем выбранное изображение в UI.Image
        imageComponent.sprite = randomImage;

        // Устанавливаем соответствующий текст в Text
        textComponent.text = randomText;
    }
}
