using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebugPlayerPrefsSaveUI : MonoBehaviour
{
    public TMP_InputField varNameInputField;
    public TMP_Dropdown typeDropdown;
    public TMP_InputField varValueInputField;
    public Button saveButton;

    private void Start()
    {
        saveButton.onClick.AddListener(SaveToPlayerPrefs);
    }

    private void SaveToPlayerPrefs()
    {
        string varName = varNameInputField.text;
        string varValue = varValueInputField.text;

        if (string.IsNullOrEmpty(varName) || string.IsNullOrEmpty(varValue))
        {
            Debug.LogError("Введите название переменной и её значение.");
            return;
        }

        // Получаем выбранный тип данных из Dropdown
        string typeName = typeDropdown.options[typeDropdown.value].text;

        // Сохраняем значение в PlayerPrefs в зависимости от выбранного типа
        switch (typeName)
        {
            case "String":
                PlayerPrefs.SetString(varName, varValue);
                break;

            case "Int":
                if (int.TryParse(varValue, out int intValue))
                {
                    PlayerPrefs.SetInt(varName, intValue);
                }
                else
                {
                    Debug.LogError("Неверный формат значения для типа Int.");
                    return;
                }
                break;

            case "Float":
                if (float.TryParse(varValue, out float floatValue))
                {
                    PlayerPrefs.SetFloat(varName, floatValue);
                }
                else
                {
                    Debug.LogError("Неверный формат значения для типа Float.");
                    return;
                }
                break;

            // Добавьте другие типы по мере необходимости

            default:
                Debug.LogError("Выбран неизвестный тип.");
                return;
        }

        PlayerPrefs.Save();
        Debug.Log($"Переменная {varName} сохранена в PlayerPrefs (тип {typeName}) с значением {varValue}.");
    }
}
