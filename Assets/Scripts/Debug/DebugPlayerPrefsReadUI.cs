using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebugPlayerPrefsReadUI : MonoBehaviour
{
    public TMP_InputField variableInputField;
    public TMP_Dropdown typeDropdown;
    public Button getValueButton;
    public TMP_Text resultText;

    private void Start()
    {
        getValueButton.onClick.AddListener(GetValueFromPlayerPrefs);
    }

    private void GetValueFromPlayerPrefs()
    {
        string variableName = variableInputField.text;
        string typeName = typeDropdown.options[typeDropdown.value].text;

        switch (typeName)
        {
            case "String":
                if (PlayerPrefs.HasKey(variableName))
                {
                    string stringValue = PlayerPrefs.GetString(variableName);
                    resultText.text = $"{variableName} ({typeName}): {stringValue}";
                }
                else
                {
                    resultText.text = $"<color=#f54545>ERR</color>: Variable {variableName} not found";
                }
                break;

            case "Int":
                if (PlayerPrefs.HasKey(variableName))
                {
                    int intValue = PlayerPrefs.GetInt(variableName);
                    resultText.text = $"{variableName} ({typeName}): {intValue}";
                }
                else
                {
                    resultText.text = $"<color=#f54545>ERR</color>: Variable {variableName} not found";
                }
                break;

            case "Float":
                if (PlayerPrefs.HasKey(variableName))
                {
                    float floatValue = PlayerPrefs.GetFloat(variableName);
                    resultText.text = $"{variableName} ({typeName}): {floatValue}";
                }
                else
                {
                    resultText.text = $"<color=#f54545>ERR</color>: Variable {variableName} not found";
                }
                break;

            default:
                resultText.text = $"<color=#f54545>ERR</color>: Unknown type {typeName}";
                break;
        }
    }
}
