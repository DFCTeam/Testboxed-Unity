using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class DebugAllVarsToTextUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textOut;
    private string currentPath;

    private void Start()
    {
    	currentPath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, "Addons"));
        textOut.text = $"Application.dataPath: {currentPath}";
    }
}
