using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObjectsOnPlatform : MonoBehaviour
{
    [SerializeField] private GameObject[] objectsToDelete;

    private void Start()
    {
		// Перебираем массив объектов и удаляем каждый из них
		if (Application.platform == RuntimePlatform.Android) {
	        foreach (GameObject objectx in objectsToDelete)
	        {
	            Destroy(objectx);
	        }
	    }
	    Destroy(this);
    }
}
