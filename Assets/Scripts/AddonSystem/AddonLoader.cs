using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEngine;
using TMPro;

[System.Serializable]
public class AddonData
{
    public string Name;
    public string id = "UnknownAuthor.UnknownName";
    public string Author;
    public string Description;
    public List<string> Tags;
    //public string assetBundleFile;
}

namespace AddonLoader
{
    public class AddonLoader : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_Object;
        [SerializeField] private GameObject ui_LoadingAddonsPanel;

        private string current_path;

        private int count_current = 0;
        private int count_all = 0;
        public List<AddonData> addons;

        private string initedAddons;

        private void Start()
        {
            Load();
        }

        private void LateUpdate()
        {
            m_Object.text = $"{count_current}/{count_all}";
            if (count_current == count_all)
            {
                ui_LoadingAddonsPanel.SetActive(false);
            }
        }

        private void Load()
        {
            // Путь к папке Addons
            string addonsFolderPath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, "Addons"));

            // Проверяем, существует ли папка Addons
            if (Directory.Exists(addonsFolderPath))
            {
                // Получаем все подпапки (каждая подпапка - отдельный аддон)
                string[] addonFolders = Directory.GetDirectories(addonsFolderPath);

                // Инициализируем список аддонов
                addons = new List<AddonData>();

                // Проходимся по каждой подпапке
                foreach (string addonFolder in addonFolders)
                {
                    // Путь к файлу Info.json внутри текущей подпапки
                    string infoFilePath = Path.Combine(addonFolder, "Info.json");

                    // Проверяем, существует ли файл Info.json
                    if (File.Exists(infoFilePath))
                    {
                        // Читаем содержимое файла
                        string jsonContent = File.ReadAllText(infoFilePath);

                        // Десериализуем JSON в объект AddonData
                        AddonData addon = JsonUtility.FromJson<AddonData>(jsonContent);
                        try {
                            addon.id = Path.GetFileName(addonFolder);
                        } catch {
                            Debug.LogError($"Can't load addon id");
                        }

                        // Добавляем аддон в список
                        addons.Add(addon);

                        // Увеличиваем счетчик всех аддонов
                        count_all++;
                    }
                    else
                    {
                        Debug.Log($"Info.json not found in {addonFolder}. Skipping addon.");
                    }
                }

                // Начинаем загрузку аддонов
                StartCoroutine(LoadAddons());
                Debug.Log($"Loaded {count_current}/{count_all} addons");
                PlayerPrefs.SetString("AddonsFirstInit", initedAddons);
            }
            else
            {
                Debug.Log("Addons folder not found!");
            }
        }

        private IEnumerator LoadAddons()
        {
            //count_all = count_all + addons.Count;

            foreach (var addon in addons)
            {
                if (addon.Name != "Testboxed") {
                    yield return StartCoroutine(LoadAddon(addon));
                }
                count_current++;
            }
        }

        public IEnumerator LoadAddon(AddonData addon)
        {
            string addonsFolderPath = Path.GetFullPath(Path.Combine(Application.persistentDataPath, "Addons"));

            // Check metadata
            // Check Addon ID
            // Valid: DFCTeam.Testboxed
            // Invalid: com.DFCTeam.Testboxed, net.Test.Dev, idk.what.whatx.wtf
            string[] addon_id_split = addon.id.Split(".");
            if (addon_id_split.Length != 2) {
                Debug.LogError($"Addon ID for {addon.Name} ({addon.id}) is incorrect");
                yield return false;
            }

            // Check name
            if (addon.Name == "") {
                Debug.LogError($"Addon name for {addon.id} is empty");
                yield return false;
            }

            string AddonsFirstInit = PlayerPrefs.GetString("AddonsFirstInit", "");
            if (AddonsFirstInit == "") { Debug.LogWarning("AddonsFirstInit is empty"); }

            string[] AddonsFirstInitSplit = AddonsFirstInit.Split(",");
            List<string> AddonsFirstInitList = new List<string>(AddonsFirstInitSplit);
            if (!AddonsFirstInitList.Contains(addon.id)) {
                // Run FirstInit.lua
                string FirstInit_lua_text = File.ReadAllText(Path.Combine(addonsFolderPath, addon.id, "Scripts", "FirstInit.lua"));
                EvalLua.EvalString(FirstInit_lua_text);
                initedAddons += addon.id;
            }

            // Run Init.lua
            string Init_lua_text = File.ReadAllText(Path.Combine(addonsFolderPath, addon.id, "Scripts", "Init.lua"));
            EvalLua.EvalString(Init_lua_text);

            // Event handling
            // Not implemented
            // string[] path = {addonsFolderPath, addon.id, "Scripts", "Events", "OnGUI.lua"};
            // string infoFilePath = Path.Combine(path);
            // if (File.Exists(infoFilePath)) {
            //     // TODO: Create new GameObject that handles it
            // }

            // Load AssetBundle
            string assetBundlePath = Path.Combine(addonsFolderPath, addon.id, "AssetBundle.unity3d");
            if (File.Exists(assetBundlePath)) {
                AssetBundle assetBundle = AssetBundle.LoadFromFile(assetBundlePath);
                
                if (assetBundle == null)
                {
                    Debug.LogWarning($"Failed to load asset bundle for addon: {addon.Name}");
                }
                else {
                    assetBundle.Unload(false);
                }
            }

            yield return true;
        }
    }
}