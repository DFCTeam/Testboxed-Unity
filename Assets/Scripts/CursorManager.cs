using UnityEngine;

public class CursorManager : MonoBehaviour
{
    private bool isCursorLocked = true;
    public GameObject cursorObject;

    [SerializeField] private Animator animator;

    private void Start()
    {
        UpdateCursorState();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isCursorLocked = !isCursorLocked;
            UpdateCursorState();
            if (animator != null) {
                if (isCursorLocked) {
                    Cursor.lockState = CursorLockMode.Locked;
                    animator.SetInteger("State", 1);
                }
                else {
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    animator.SetInteger("State", 2);
                }
            }
        }
    }

    private void UpdateCursorState()
    {
        Cursor.lockState = isCursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !isCursorLocked;

        if (cursorObject != null)
        {
            cursorObject.SetActive(!isCursorLocked);
        }
    }
}