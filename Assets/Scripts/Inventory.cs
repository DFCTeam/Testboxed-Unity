// From original Testboxed
// We should reimplement it

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Inventory : NetworkBehaviour
{
    [SerializeField] private GameObject[] items;
    [SerializeField] private GameObject parentObject; // SpawnPoint
    [SerializeField] private int currentObject = 0;

    private void ClearChilds(Transform x) {
        int childs = items.Length + 1;

        for (int i = childs - 1; i > 0; i--)
        {
            try
            {
                GameObject.Destroy(x.GetChild(i).gameObject);
            }
            catch
            {

            }
        }
    }

    private void SpawnObject()
    {
        ClearChilds(parentObject.transform);
        var newobj = Instantiate(
            items[currentObject],
            new Vector3(parentObject.transform.position.x,parentObject.transform.position.y, parentObject.transform.position.z),
            new Quaternion(0.0f,0.0f,0.0f,0.0f),
            parentObject.transform
        );
    }

    private void Update()
    {
        if (!isLocalPlayer) return;

        if (!Input.GetKey(KeyCode.RightControl)) {
            var mouseScroll = Input.mouseScrollDelta.y;
            if (mouseScroll == 1) {
                if (this.currentObject < (this.items.Length - 1)) {
                    this.currentObject = this.currentObject + 1;
                    SpawnObject();
                }
            }
            if (mouseScroll == -1) {
                if (this.currentObject > 0) {
                    this.currentObject = this.currentObject - 1;
                    SpawnObject();
                }
            }
        }
    }
}