using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MPAttachCamera : NetworkBehaviour
{
    // Multiplayer is removed

    public Camera myCamera;

    private void Start() {
        myCamera.depth = 0;

        if (isLocalPlayer) {
            myCamera.depth = 1;
        }
    }
}
