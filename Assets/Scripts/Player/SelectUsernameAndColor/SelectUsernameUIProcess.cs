using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectUsernameUIProcess : MonoBehaviour
{
    [SerializeField] private GameObject gj;

    [SerializeField] private TMP_InputField usernameInput;
    [SerializeField] private TMP_Dropdown colorsDropdown;
    [SerializeField] private Button confirmButton;

    private void Start()
    {
        LoadPlayerPrefs(); // Загружаем сохраненные значения при старте
        confirmButton.onClick.AddListener(SavePlayerPrefs); // Добавляем слушателя на кнопку подтверждения
    }

    // Метод для сохранения значений в PlayerPrefs
    private void SavePlayerPrefs()
    {
        // Сохраняем имя пользователя
        PlayerPrefs.SetString("PlayerUsername", usernameInput.text);

        // Сохраняем выбранный цвет по его индексу в Dropdown
        PlayerPrefs.SetInt("PlayerColor", colorsDropdown.value+1);

        // Сохраняем PlayerPrefs
        PlayerPrefs.Save();

        gj.SetActive(false);
    }

    // Метод для загрузки значений из PlayerPrefs
    private void LoadPlayerPrefs()
    {
        // Загружаем имя пользователя
        string savedUsername = PlayerPrefs.GetString("PlayerUsername", "");
        usernameInput.text = savedUsername;

        // Загружаем индекс выбранного цвета
        int savedColorIndex = PlayerPrefs.GetInt("PlayerColor", 0);
        colorsDropdown.value = savedColorIndex;

        // Получаем имя выбранного элемента в Dropdown
        string selectedColorName = GetSelectedColorName();
        Debug.Log("Selected Color: " + selectedColorName);
    }

    // Метод для получения имени выбранного элемента в Dropdown
    private string GetSelectedColorName()
    {
        TMPro.TMP_Dropdown.OptionData selectedOption = colorsDropdown.options[colorsDropdown.value];
        return selectedOption.text;
    }
}