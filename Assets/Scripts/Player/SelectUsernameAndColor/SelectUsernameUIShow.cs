using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectUsernameUIShow : MonoBehaviour
{
    [SerializeField] private GameObject gj;

    private void Start()
    {
        string username = PlayerPrefs.GetString("PlayerUsername", "");
        int color = PlayerPrefs.GetInt("PlayerColor", 0);
        if (
            (username == "") ||
            (color == 0)
        ) {
            gj.SetActive(true);
        }
    }
}
