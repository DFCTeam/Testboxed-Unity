using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTexturesWithInterval : MonoBehaviour
{
    public Sprite[] sprites;  // Массив спрайтов для изменения
    private SpriteRenderer spriteRenderer;   // Компонент SpriteRenderer объекта
    private int currentIndex = 0; // Индекс текущего спрайта
    public float changeInterval = 0.3f; // Интервал смены спрайта

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        // Проверяем, что у объекта есть компонент SpriteRenderer
        if (spriteRenderer == null)
        {
            Debug.LogError("SpriteRenderer component not found on this object.");
            enabled = false; // Отключаем скрипт
        }

        // Проверяем, что массив спрайтов не пуст
        if (sprites == null || sprites.Length == 0)
        {
            Debug.LogError("Sprites array is empty or null.");
            enabled = false; // Отключаем скрипт
        }

        // Вызываем метод для смены спрайтов с интервалом
        InvokeRepeating("ChangeSpriteWithInterval", 0f, changeInterval);
    }

    private void ChangeSpriteWithInterval()
    {
        // Проверяем, что индекс не выходит за пределы массива
        if (currentIndex < sprites.Length)
        {
            // Устанавливаем текущий спрайт
            spriteRenderer.sprite = sprites[currentIndex];

            // Увеличиваем индекс для следующего спрайта
            currentIndex++;
        }
        else
        {
            // Если достигнут конец массива, начинаем сначала
            currentIndex = 0;
        }
    }
}
