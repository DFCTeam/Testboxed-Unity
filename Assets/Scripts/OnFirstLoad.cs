using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

#if UNITY_EDITOR
    using UnityEditor;
    using NUnit.Framework;
#endif

public class OnFirstLoad : MonoBehaviour
{
    [SerializeField] private GameObject sceneLoader;
    [SerializeField] private TMP_Text loadingText;

    // This script will be called if Testboxed started firstly on this PC
    // or if old version != new version
    private void Start()
    {
        string LoadedFirst = PlayerPrefs.GetString("LoadedFirst", "No");
        Debug.Log(LoadedFirst);

        if (LoadedFirst == "No") {
            sceneLoader.SetActive(false);
            #if UNITY_EDITOR
                loadingText.text = "Compiling shaders...";
                CompileShaders();
            #endif
        }
    }

    private void CompileShaders()
    {
        #if UNITY_EDITOR
            var guids = AssetDatabase.FindAssets("t:shader");
            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var shader = AssetDatabase.LoadAssetAtPath<Shader>(path);
                Assert.IsTrue(shader, "Could not load shader at '{0}'.", path);
    
                var hasError = ShaderUtil.ShaderHasError(shader);
                Assert.IsFalse(hasError, "Shader '{0}' has errors.", path);
    
                var material = new Material(shader);
                int passCount = material.passCount;
                for (int i = 0; i < passCount; i++)
                {
                    ShaderUtil.CompilePass(material, i, true);
                }
                Object.DestroyImmediate(material);
            }
        #endif
    }
}
