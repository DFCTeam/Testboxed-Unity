using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneByName : MonoBehaviour
{
    [SerializeField] private string sceneName;

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadSceneAsync(sceneName);
    }

    private void Start()
    {
        StartCoroutine(Delay());
    }
}
