using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoonSharp.Interpreter;

public class LuaTest : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log(MoonSharpFactorial());
    }

	private bool MoonSharpFactorial()
	{
		string script = @"    
			-- defines a factorial function
			function fact (n)
				if (n == 0) then
					return 1
				else
					return n*fact(n - 1)
				end
			end

			return fact(50)";

		DynValue res = Script.RunString(script);
		return true;
		//return res.Number;
	}
}
