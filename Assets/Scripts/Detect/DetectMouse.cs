using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectMouse : MonoBehaviour
{
    [SerializeField] private GameObject objectWithMouse;
    [SerializeField] private GameObject objectWithoutMouse;

    private void Update()
    {
        // Проверяем, есть ли мышь
        if (Input.mousePresent)
        {
            // Если мышь есть, выключаем объект без мыши и включаем объект с мышью
            objectWithMouse.SetActive(true);
            objectWithoutMouse.SetActive(false);
        }
        else
        {
            // Если мыши нет, выключаем объект с мышью и включаем объект без мыши
            objectWithMouse.SetActive(false);
            objectWithoutMouse.SetActive(true);
        }
    }
}
