using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromoUIButton : MonoBehaviour
{
    [SerializeField] private GameObject gj;
    [SerializeField] private Button btn;
    [SerializeField] private string varName;

    private void Start()
    {
        btn.onClick.AddListener(onClick);
    }
    private void onClick()
    {
        PlayerPrefs.SetInt(varName, 1);
        PlayerPrefs.Save();
        gj.SetActive(false);
    }
}
