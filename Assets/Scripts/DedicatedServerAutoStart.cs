using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DedicatedServerAutoStart : MonoBehaviour
{
    private UnityEngine.RuntimePlatform[] arrServer = new UnityEngine.RuntimePlatform[3];

    private void Start()
    {
        arrServer[0] = UnityEngine.RuntimePlatform.WindowsServer;
        arrServer[1] = UnityEngine.RuntimePlatform.LinuxServer;
        arrServer[2] = UnityEngine.RuntimePlatform.OSXServer;

        if (Array.IndexOf(arrServer, Application.platform) != -1) {
            NetworkManager.singleton.StartServer();
            Debug.Log("Server started");
        }
    }
}
