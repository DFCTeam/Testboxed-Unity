from PIL import Image
import os

def split_textures(input_image_path, output_folder):
    img = Image.open(input_image_path)

    width, height = img.size

    texture_size = 128#256#128

    for y in range(0, height, texture_size):
        for x in range(0, width, texture_size):
            left = x
            top = y
            right = x + texture_size
            bottom = y + texture_size

            texture = img.crop((left, top, right, bottom))

            output_path = f"{output_folder}/texture_{x}_{y}.png"
            texture.save(output_path)
            print(f"{output_folder}/texture_{x}_{y}.png <- {input_image_path}")

if __name__ == "__main__":
    gen = []

    for x in range(1,9):
        #print(x)
        gen.append(f"0{x}")

    for x in range(10,25):
        #print(x)
        gen.append(f"{x}")

    for x in gen:
        input_image_path = f"./Input/{x}.png"
        output_folder = f"./Output/{x}"

        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        split_textures(input_image_path, output_folder)