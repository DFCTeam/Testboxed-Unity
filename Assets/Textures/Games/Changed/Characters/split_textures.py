from PIL import Image

def split_textures(input_image_path, output_folder):
    img = Image.open(input_image_path)

    width, height = img.size

    texture_size = 256#128

    for y in range(0, height, texture_size):
        for x in range(0, width, texture_size):
            left = x
            top = y
            right = x + texture_size
            bottom = y + texture_size

            texture = img.crop((left, top, right, bottom))

            output_path = f"{output_folder}/texture_{x}_{y}.png"
            texture.save(output_path)

if __name__ == "__main__":
    input_image_path = "./Input/22.png"
    output_folder = "./Output/22"

    split_textures(input_image_path, output_folder)